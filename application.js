// Import the Cantabile library
// See documentation here: http://www.cantabilesoftware.com/jsapi
//const Cantabile = require('../cantabile-js/CantabileApi.js');
const Cantabile = require('cantabile-js');

// Create a Cantabile object and tell it to connect
const C = new Cantabile();
C.on('stateChanged', state => console.log(`CONN STATE: ${state}`));
C.connect();

C.application.open();
C.application.on('changed', () => console.log(C.application.colors));
