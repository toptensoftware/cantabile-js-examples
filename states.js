// Import the Cantabile library
// See documentation here: http://www.cantabilesoftware.com/jsapi
const Cantabile = require('cantabile-js');

// Create a Cantabile object and tell it to connect
const C = new Cantabile();
C.on('stateChanged', state => console.log(`STATE: ${state}`));
C.connect();

// Open the set list connection (no longer necessary)
//C.songStates.open();

// Listen to the set list change and the current song change events and 
// when either triggers, call the dumpStates function
C.songStates.on('changed', dumpStates);
C.songStates.on('currentStateChanged', dumpStates);

// Helper to dump the current set list
function dumpStates() 
{
    if (C.songStates.items === null)
        return;
        
    console.log(`===== ${C.songStates.name} =====\n`)

    let items = C.songStates.items;
    for (let i=0; i<items.length; i++)
    {
        let item = items[i];
        if (item === C.songStates.currentState)
            console.log(`==> ${item.pr} - ${item.name}`); 
        else
            console.log(`    ${item.pr} - ${item.name}`); 
    }

    console.log("\n\n");
}


// Run this program and then try editing the set list or changing the currently active song