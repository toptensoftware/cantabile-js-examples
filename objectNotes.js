// Import the Cantabile library
// See documentation here: http://www.cantabilesoftware.com/jsapi
const Cantabile = require('cantabile-js');

// Create a Cantabile object and tell it to connect
var C = new Cantabile();
C.on('stateChanged', state => console.log(`STATE: ${state}`));
C.connect();

// Open the bindings connection
C.bindings.open();

// A full list of bindings points is available here: (assuming Cantabile is running on your local machine)
//   http://localhost:35007/api/bindings/availableBindingPoints 

// Watch the user notes field on the first 4 objects in the current song
for (let i=0; i<4; i++)
{
    C.bindings.watch(
        "global.indexedGenericObject.notes",    // The source binding point
        [0,i],                                  // 0 means the song, i is the index of which object to watch
        null,                                   // No condition
        function(value) {
            // This function will get called whenever the notes for object `i` change
            console.log(`Object ${i} = "${value}"`);
        }
    );
}

