// Import the Cantabile library
// See documentation here: http://www.cantabilesoftware.com/jsapi
const Cantabile = require('cantabile-js');

// Create a Cantabile object and tell it to connect
const C = new Cantabile();
C.on('stateChanged', state => console.log(`STATE: ${state}`));
C.connect();

// Open the set list connection (no longer necessary)
//C.setList.open();

// Listen to the set list change and the current song change events and 
// when either triggers, call the dumpSetList function
C.setList.on('changed', dumpSetList);
C.setList.on('currentSongChanged', dumpSetList);
C.setList.on('currentSongPartChanged', (part, partCount) => console.log(`SONG Part ${part+1} of ${partCount}`));

// Helper to dump the current set list
function dumpSetList() 
{
    console.log(`===== ${C.setList.name} =====\n`)

    let items = C.setList.items;
    for (let i=0; i<items.length; i++)
    {
        let item = items[i];
        if (item.kind =='break')
            console.log(`     ----- ${item.name} -----`);
        else
        {
            if (item === C.setList.currentSong)
                console.log(`==> ${item.pr} - ${item.name}`); 
            else
                console.log(`    ${item.pr} - ${item.name}`); 
        }
    }

    console.log("\n\n");
}


// Run this program and then try editing the set list or changing the currently active song