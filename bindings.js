// Import the Cantabile library
// See documentation here: http://www.cantabilesoftware.com/jsapi
const Cantabile = require('cantabile-js');

// Create a Cantabile object and tell it to connect
var C = new Cantabile();
C.on('stateChanged', state => console.log(`STATE: ${state}`));
C.connect();

// Open the bindings connection (no longer necessary)
//C.bindings.open();

// A full list of bindings points is available here: (assuming Cantabile is running on your local machine)
//   http://localhost:35007/api/bindings/availableBindingPoints 

// Watch for CC 16 on the onscreen keyboard
C.bindings.watch(
    "midiInputPort.Onscreen Keyboard",                      // The source binding point
    null,                                                   // No indicies needed
    { kind: "Controller", controller: 16, channel: 0 },     // The event to watch for (CC 16 on channel 1)
    onCC16                                                  // The function to call when triggered
);

// This function will be called whenever CC 16 changes
function onCC16(value)
{
    // Show what's happening
    console.log("CC 16: ", value);

    // Invoke  the master output level binding to adjust the gain
    C.bindings.invoke("global.masterLevels.outputGain", value / 127);
}
