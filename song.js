// Import the Cantabile library
// See documentation here: http://www.cantabilesoftware.com/jsapi
const Cantabile = require('cantabile-js');

// Create a Cantabile object and tell it to connect
const C = new Cantabile();
C.on('stateChanged', state => console.log(`CONN STATE: ${state}`));
C.connect();

// Listen to the set list change and the current song change events and 
// when either triggers, call the dumpSetList function
C.song.on('changed', () => console.log(`SONG: ${C.song.name}`));
C.song.on('currentStateChanged', () => console.log(`SONG STATE: ${C.song.currentState}`));

// Run this program and then try changing the currently active song