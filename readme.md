# Cantabile JavaScript Examples

This repository contains various examples of how to work with Cantabile's JavaScript API.

## Getting Started

1. Make sure you have [Node.JS version 8](https://nodejs.org/en/download/) or later installed.
2. Make sure you have [Git](https://git-scm.com/downloads) installed.
3. Bring up a Windows command prompt (Start -> Run -> cmd.exe)
4. Clone the git repository:

        > git clone https://toptensoftware@bitbucket.org/toptensoftware/cantabile-js-examples.git

5. Change to the examples project

        > cd cantabile-js-examples

6. Run the `npm install` command:

        > npm install

7. Run any of the example programs, using `node`.  eg: to run the bindings example:

        > node bindings

8. To get the latest version of the examples, change to the same directory and run git pull:

        > git pull

