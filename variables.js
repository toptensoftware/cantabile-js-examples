// Import the Cantabile library
// See documentation here: http://www.cantabilesoftware.com/jsapi
const Cantabile = require('cantabile-js');

// Create a Cantabile object and tell it to connect
var C = new Cantabile();
C.on('stateChanged', state => console.log(`STATE: ${state}`));
C.connect();

// Open the bindings connection (no longer necessary)
//C.variables.open();

// Watch a string with embedded "Cantabile" variables:
// For a full list of available variables, see here: http://www.cantabilesoftware.com/guides/variables
C.variables.watch("The current song is $(SongTitle)", function(value) {
    console.log(value);
});

// Another example
C.variables.watch("Transport: $(TransportState)", function(value) {
    console.log(value);
});

C.variables.watch("$(Tempo) bpm", function(value) {
    console.log(value);
});


// Run this example and then try changing songs and starting/stopping the transport in Cantabile